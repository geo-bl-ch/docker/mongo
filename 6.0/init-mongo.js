if (
    process.env.MONGO_INITDB_ROOT_USERNAME
    && process.env.MONGO_INITDB_ROOT_PASSWORD
    && process.env.MONGODB_DATABASE
    && process.env.MONGODB_USER
    && process.env.MONGODB_PASSWORD
 ) {

    db.getSiblingDB('admin').auth(
        process.env.MONGO_INITDB_ROOT_USERNAME,
        process.env.MONGO_INITDB_ROOT_PASSWORD
    );

    db = db.getSiblingDB(process.env.MONGODB_DATABASE)

    db.createUser({
        user: process.env.MONGODB_USER,
        pwd: process.env.MONGODB_PASSWORD,
        roles: [{role: "readWrite", db: process.env.MONGODB_DATABASE}]
    });
}


