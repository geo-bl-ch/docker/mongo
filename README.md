# mongo

([MongoDB-Image on Docker-Hub](https://hub.docker.com/_/mongo))

Mongo database image extended by the feature that an additional non-admin user authorized for a specific database can be created on initialisation.

| Environment variable | Remark |
| -- | -- |
| MONGO_INITDB_ROOT_USERNAME | same meaning as in base image |
| MONGO_INITDB_ROOT_PASSWORD | same meaning as in base image |
| MONGO_INITDB_DATABASE | same meaning as in base image |
| MONGODB_DATABASE | new database to be created |
| MONGODB_USER | username of user with readWrite permission in database MONGODB_DATABASE |
| MONGODB_PASSWORD | password of user with readWrite permission in database MONGODB_DATABASE |
